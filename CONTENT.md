## Minds Content Policy

The goal of Minds is to have fair, transparent and ethical moderation practices based on United States law and the [Santa Clara Principles](https://santaclaraprinciples.org). 

The current moderation system functions as follows:

(Insert Image of Flow)

**Minds Jury System**

A Jury consists of 12 randomly selected unique active Minds users who are not subscribed to the user under review. Each juror selected is provided with the option to participate, pass or opt-out of the Jury pool entirely. 

If 75% or more of the Jury members vote to accept the appeal of a strike, the administrative action is overturned. If less than 75% of the Jury members vote accept the appeal, the administrative action is upheld. The decision of the Jury will be final.

**Strike Offense**

Users will receive a strike for posting or linking to certain content. Users will be notified about the strike, which term was violated, and which piece of content was in violation. All strikes can be appealed to a Jury. Individual strikes will expire after 90 days. 

The following violations will result in a strike:

Untagged NSFW Post 

* Strike 1 = Warning
* Strike 2 = 2nd Warning
* Strike 3 = Full channel marked with NSFW category

NSFW (not safe for work) is defined as content containing nudity, pornography, profanity, violence, gore, or sensitive commentary on race, religion, or gender. In general terms, it is defined as content which a reasonable viewer may not want to be seen accessing, in a public setting, such as in a workplace.  These tags can be applied to individual content or any group or channel. The full channel will not be marked with a NSFW category until it has received 3 strikes in a single NSFW category.


Harassment or Spam

* Strike 1 = Warning
* Strike 2 = 2nd Warning
* Strike 3 = Ban

NSFW (not safe for work) is defined as content containing nudity, pornography, profanity, violence, gore, or sensitive commentary on race, religion, or gender. In general terms, it is defined as content which a reasonable viewer may not want to be seen accessing, in a public setting, such as in a workplace.  These tags can be applied to individual content or any group or channel.

Spam on Minds is generally defined as repeated, unwanted, and/or unsolicited actions, automated or manual, that negatively affect Minds users, groups, and/or Minds itself. Spam also includes content that is designed to further unlawful acts (such as phishing) or mislead recipients as to the source of the material (such as spoofing). Spam may result in an immediate ban if determined to be malicious or by use of a bot.

**Immediate Ban Offense**

Users will be immediately banned for posting or linking to certain content. Users will be notified about the ban and which term was violated, but they will not be able to see the content that was in violation as it will have been removed from Minds. Appeals on immediate bans will be reviewed by the Minds admins and not a Jury due to the nature of the content.

Illegal Content
* Promotion or glorification of terrorism
* Extortion
* Fraud
* Child pornography, advocating pedophilia or any relating harm to a child
* Posting nude or sexually explicit photos of someone you know without his or her permission
* Sex trafficking or the promotion of prostitution, human trafficking or illegal sexual conduct

Other Content
* Personal and confidential information (doxxing), content that violates the privacy or publicity rights or another person, or other stalking behavior
* Impersonation, or otherwise misleading readersi nto thinking that you are another person or entity
* Theats or incitement of violence
* Viruses, worms, malware, rojan horses or similar harmful or destructive content
* Token manipulation, or otherwise using unfair or fraudulent methods to obtain Minds Tokens

**Boost Policy**

Boost is the advertising network on Minds. Users may exchange Minds Tokens to “Boost” their content and receive views from the network. All boosted content is manually reviewed by Minds admins. Users may not Boost any content that is in violation of this Content Policy. Additionally, users may not Boost content that: 

* Contains spam or any content that results in an immediate ban offense, as listed above;
* Appeals a moderation decision;
* Exceeds Boost rate limits;
* Is a Help & Support request; 
* Is selling Minds Tokens;
* Contains content from a banned user;
* Failed onchain payment; or
* Has been removed

If you would like to appeal a Boost rejection, you may do so by emailing info@minds.com with the post in question and the reasons for why you believe it should be accepted. Boost appeals are currently reviewed by Minds admins, whose decision will be final.
